import axios from 'axios'

const API_URL = 'football.jsonc'

export function getBoard(){
  return axios.get(API_URL)
}
