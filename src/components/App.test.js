import { render, screen } from '@testing-library/react'
import ReactDOM from 'react-dom';
import App from './App'

it('renders without crashing div', () => {
  ReactDOM.render(<App />, document.createElement('div'));
});

it('renders without crashing p', () => {
  ReactDOM.render(<App />, document.createElement('p'));
});

it('renders without crashing p', () => {
  ReactDOM.render(<App />, document.createElement('span'));
});

/*test('renders Ateam F.C label', () => {
  render(<App />)
  expect(screen.getByText('Ateam F.C')).toBeInTheDocument()
});

test('renders A.C Bteam label', () => {
  render(<App />)
  expect(screen.getByText('A.C Bteam')).toBeInTheDocument()
});*/
