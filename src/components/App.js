import { useEffect, useState } from 'react'
import { getBoard } from '../services/FootballService'
import './App.scss'

const Line = ( { quantity } ) => {
  let breakpoints = []
  for (var i = 0; i < quantity; i++) { 
    breakpoints[i] = <div key = {i}  className = "breakpoint"/>
  }

  return (
    <div>
      <div className = "underlay">
        { breakpoints.map( breakpoint => { return breakpoint }) }
      </div>
    </div>
  )
}

const Dot = ( { actions } ) => {
  return (
    <div>
      {
        actions.map( (action) => {
          return (action.side === 'home') 
            ? <span key = {action.side + action.timeStamp}  className = 'home-dot'  id = {action.side + action.timeStamp}  style = {{left: (action.timeStamp + 22) + 'px'}}/>
            : <span key = {action.side + action.timeStamp}  className = 'away-dot'  id = {action.side + action.timeStamp}   style = {{left: action.timeStamp + 'px'}}/>
        })
      }
    </div>
  )
}

const App = () => {
  const [game, setGame] = useState([])

  const loadGame = () => {
    getBoard()
      .then(response => { setGame(response.data) })
      .catch(e => { console.log(e) })
  }

  useEffect(() => {
    loadGame();
  }, [])

  return (
    <div className = "App">
        { game.competitors && <p className = "home"> {game.competitors[0].name} </p> }
        <div className = "timeline">

          <Line  quantity = {9}/>

          <div className = "bar"/>

          { game.actions && <Dot  actions = { game.actions }/> }

        </div>
        { game.competitors && <p className = "away"> {game.competitors[1].name} </p> }
    </div>
  )
}

export default App
